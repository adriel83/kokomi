const Discord = require('discord.js');
const client = new Discord.Client();
const StringDecoder = require('string_decoder').StringDecoder;
const http = require('http');
const port = 3000;

client.once('ready', () => {
});

client.on('error', error =>{
    client.destroy().then(() => {
        console.log("Ocorreu um erro, "+error);
    });
});

client.on('message', message =>
{
    if(message.content === 'teste'){
        message.channel.send(message.channel.id);
    }
});

client.login('token').then( ()=>
    console.log("Loguei")
);

const httpServer = http.createServer((request) => {
    if(request.type === 'POST'){
        const decoder = new StringDecoder('utf-8');
        let datapost = '';
        request.on('data', (data) => {
            datapost += decoder.write(data);
        });
        request.on('end', () => {
            datapost += decoder.end();
            datapost = JSON.parse(datapost);
            let channel = client.channels.find('id', 'id do canal');
            request.end();
            return channel.send('@everyone ' + datapost.url);
        });
    }
});
httpServer.listen(port, () => {
    console.log(`Porta: ${port}`);
});